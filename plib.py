

from termcolor import colored, cprint
import io
from traceback import print_stack, extract_stack, format_list
import re
from sys import stdout

_disable_debug_print = True

def enable_debug_print():
    global _disable_debug_print
    _disable_debug_print = False

_starttime_print = None

def _print(*args, **kwargs):        

    limit = None

    if _disable_debug_print and "appendlog" not in kwargs: 
        return     
        
    cl = format_list(extract_stack())
    cl.pop(-1) # remove last 
    last = cl[-1]
    cl = cl[kwargs.get('stack',-1):]    

    global _starttime_print
        
    if 'start' in kwargs: 
        _starttime_print = datetime.datetime.now()
        kwargs.pop('start')
    if 'stop' in kwargs: 
        _starttime_print = None    
        kwargs.pop('stop')

    if 'stack' in kwargs: kwargs.pop('stack')

    if not cl: cl = [ last ]
    cl = [ re.sub(r"/home/\w+/","~",i.split("\n")[0]) for i in cl ]      
    last = cl.pop(-1)    

    f = stdout

    appendlog = kwargs.pop("appendlog",None)
    if appendlog:
        f = open(appendlog,"a")


    for i in cl: print(i,file=f)
    
    cprint("%-70s "%last.strip(),end="|",file=f)
    if _starttime_print: 
        cprint("%-10s"%(datetime.datetime.now() - _starttime_print),'red', end='| ',file=f)

    output = io.StringIO()  
    color = 'green'
    if 'color' in kwargs:
        color = kwargs.pop('color')  
    print(*args, **kwargs, file=output)
    contents = output.getvalue()
    output.close()
    
    cprint(contents, color, file=f, end='')
    f.flush()

    if appendlog: f.close()