from plib import _print, enable_debug_print
import subprocess, itertools, sys

Alabel = "L"
Blabel = "R"
ABset = frozenset([Alabel,Blabel])
Aset = frozenset([Alabel])
Bset = frozenset([Blabel])

def ps(s): return "".join(list(s))
def psc(s): return ",".join(list(s))
def psx(*args): 
    return "|".join(psc(c) for c in args)

def aset(X): return Alabel in X and Blabel not in X
def bset(X): return Blabel in X and Alabel not in X
def abset(X): return Alabel in X and Blabel in X
def zeroset(X): return Alabel not in X and Blabel not in X

def varphi1(C,D,X):
    if (aset(C) and bset(D) or bset(C) and aset(D)) and not C.intersection(D):
        return 2**(len(X.difference(C.union(D))))
    return 0

def ints(X,Y,Z):
    return 2**len(X.intersection(Y).intersection(Z))

def difflen(X,Y):
    return len(X.difference(Y))

def difflen2(X,Y):
    return 2**len(X.difference(Y))


def getureconnectivitydata(genetree):
    try:            
        ### Print edges connectivity graph, i.e., nodes = edges (rooting ids); edge = if two edges are adjacent
        #./urec -g '(a:2,b:3,(d:1,(e,f)))' -rc 
        # 2 0 1 0 1 2 8 2 5 2 5 8 7 8 6 8 6 7

        p = subprocess.Popen(["./urec/urec","-g",genetree,"-rc" ], stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
        out, err = p.communicate()
        return out.decode("utf8").strip().split("\n")            
                       
    except Exception as e:
        print("Cannot execute urec",e)
        exit(-1)

class EdgeDistance:

    def __init__(self, genetree, ureconnectivitydata = None):

        if not ureconnectivitydata:
            ureconnectivitydata = getureconnectivitydata(genetree)
                
        self.E=[ eval(e.replace(" ",",")) for e in ureconnectivitydata ]

        self.V=set()
        for a,b in self.E:
            self.V.add(a)
            self.V.add(b)
        self.V=sorted(list(self.V))
        maxd = len(self.V)+1        
        
        dist={}
        for a,b in itertools.product(self.V,self.V): dist[a,b]=maxd
        for a in self.V: dist[a,a]=0
        for a,b in self.E: dist[a,b]=dist[b,a]=1
        for k,i,j in itertools.product(self.V,self.V,self.V):    
            if dist[i,j] > dist[i,k] + dist[k,j]:
                dist[i,j]=dist[i,k] + dist[k,j]
        self.dist = dist

        # histogram of distances in the connectivity graph
        self.hist=[0]*len(self.V)
        for a,b in self.dist:
            if a<=b: 
                self.hist[dist[a,b]]+=1            
        while self.hist[-1]==0: self.hist.pop()

    # distances between eid and set of eids
    def minavgmaxposstats(self,eid,besteids):
        dmin = min(self.dist[eid,e] for e in besteids)
        davg = sum(self.dist[eid,e] for e in besteids)/len(besteids)
        dmax = max(self.dist[eid,e] for e in besteids)
        return dmin,davg,dmax

    # diameter of the connectivity graph
    def ediameter(self):
        return max(i for i in range(len(self.hist)) if self.hist[i]>0)

    # connectivity histogram repr.
    def histstr(self):        
        return ";".join(str(i) for i in self.hist)

def geturecrootingdata(genetree):
    try:            
        p = subprocess.Popen(["./urec/urec","-g",genetree,"-rrxt" ], stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
        out, err = p.communicate()
        return out.decode("utf8").strip().split("\n")            
                       
    except Exception as e:
        print("Cannot execute urec: please download and compile\ngit clone git@bitbucket.org:pgor17/urec.git",e)
        exit(-1)


def gtsupport(genetree, deltaLeft, deltaRight, skipzeros, urecrootingdata = None):
    """
    Support generator for edges of gene tree. 
    Returned items are tuples:
    *  gene tree edge id
    *  the contribution of weight from no-arrow edges
    *  the contribution of weight from one-arrowe edges
    *  the contribution of weight from double edges
    *  the weight of the edge
    *  bool: True iff the edge is the original rooting edge 
    """

    if not urecrootingdata:
        urecrootingdata = geturecrootingdata(genetree)


    def convert(x):
        for elt in x:
            if elt in deltaLeft: 
                yield Alabel
            elif elt in deltaRight:
                yield Blabel
            else: 
                yield elt

    # extract quadruple info from urec edges output
    dur = {}
    for i in urecrootingdata:

        a,b = i.split("[")
        rid = int(a.split()[0].split("=")[1])
        
        b,c = b.split("]")
        C,D = b.split("|")      
        C=C.strip().split(":")
        D=D.strip().split(":")        

        start = 'start' in c

        CL = [ set(convert(e.strip().split(' '))) for e in C ]
        DL = [ set(convert(e.strip().split(' '))) for e in D ]

        if len(CL)==1: C=CL[0]
        else: C=CL[0].union(CL[1])

        if len(DL)==1: D=DL[0]
        else: D=DL[0].union(DL[1])

        dur[rid] = (CL,DL,C,D,start)

    # crop deltaLeft and deltaRight
    genetreelabels = C.union(D) 

    if Alabel not in genetreelabels or Blabel not in genetreelabels:
        raise Exception(f"Both largest supports {Alabel}='{psc(deltaLeft)}' and {Blabel}='{psc(deltaRight)}' elements must be present in a gene tree. Gene tree labels after convertion {psc(genetreelabels)}")
    
    #v16
    def Sa(C,Di,X):
        Ca=C.difference(Aset)   
        XmDi = X.difference(Di) 
        return int (Alabel not in Di)* int( Ca.issubset(XmDi)) * difflen2(X,C.union(Di))
        
    def Sb(C,Di,X):
        Ca=C.difference(Aset)       
        return int(Blabel not in Di) * difflen2(X,C.union(Di))
        #return int(Blabel not in Di)*int( Ca.issubset(Di) ) * 2**len(Di.intersection(XmC)) # v15

        
    #???
    def fa(C,D1,D2,X):

        D1uD2 = D1.union(D2)        
        D1uD2uC = D1uD2.union(C)        
        df = difflen2(X,D1uD2uC)

        #v16
        r1 = int(Alabel not in D1uD2) * int( len(X.intersection(D1uD2.intersection(C)))==0 ) * df

        #v16
        Di,Dj=D1,D2     
        r2 = int(Alabel not in Di) * int(Blabel not in Dj) * int( len(X.intersection(Di.intersection(C.union(Dj))))==0 ) * df
        #v16
        Di,Dj=D2,D1 
        r3 = int(Alabel not in Di) * int(Blabel not in Dj) * int( len(X.intersection(Di.intersection(C.union(Dj))))==0 ) * df

        # i1 = int(Alabel in C and len(C)==1)*int(Alabel in D2.difference(D1))*int(Blabel not in D1)
        # i2 = int(Alabel in D1.difference(D1))*int(Blabel not in D2)

        #================================================

        sa1=Sa(C,D1,X)
        sa2=Sa(C,D2,X)
        sb1=Sb(C,D1,X)
        sb2=Sb(C,D2,X)

        _print (f"\tIn f_a(C={ps(C)}, D1={ps(D1)}, D2={ps(D2)})") #
        _print (f"\t|  Sa(C,D1)={sa1} Sa(C,D2)={sa2} Sb(C,D1)={sb1} Sb(C,D2)={sb2}") #

        _print (f"\t|  Sa(C,D1)&Sa(C,D2)={r1} Sa(C,D1)&Sb(C,D2)={r2} Sa(C,D2)&Sb(C,D1)={r3}")
        #print (f"\t|  Sa(C,D1)&Sa(C,D2)={r1} Sa(C,D1)&Sb(C,D2)={r2} Sa(C,D2)&Sb(C,D1)={r3} I...={i1} I...={i2}")
        
        x,y = sa1+sa2+sb1+sb2, r1+r2+r3 
        _print (f"\t--> f_a(C={ps(C)}, D1={ps(D1)}, D2={ps(D2)})={x}-{y}={x-y}")

        return x-y

    #v16
    def Ra(C,Di,X):     
        return int(Alabel not in Di) * difflen2(X,C.union(Di))
        
    #v16    
    # def Rb(C,Di,X):       
    #   return int(Blabel not in Di) * difflen2(X,C.union(Di))

    #v17    
    def Rb(C,Di,X):     
        return int(Blabel not in Di) * int( len(Di.intersection(X.intersection(C)))==0 )  * difflen2(X,C.union(Di))

    def fb(C,D1,D2,X):

        D1uD2 = D1.union(D2)
        df = difflen2(X,C.union(D1uD2))

        
        e1 = int( len(D1.intersection(X.intersection(C)))==0 )
        e2 = int( len(D2.intersection(X.intersection(C)))==0 )
        r1 = int (Blabel not in D1uD2) * e1 * e2  * df

        Di, Dj = D1, D2
        
        r2 = int (Alabel not in Di) * int(Blabel not in Dj) * int ( len(Dj.intersection(X.intersection(C.union(Di))))==0)  * df

        

        Di, Dj = D2, D1
        r3 = int (Alabel not in Di) * int(Blabel not in Dj) * int ( len(Dj.intersection(X.intersection(C.union(Di))))==0)  * df        
        
        
        ra1=Ra(C,D1,X)
        ra2=Ra(C,D2,X)
        rb1=Rb(C,D1,X)
        rb2=Rb(C,D2,X)

        _print (f"\tIn f_b(C={ps(C)}, D1={ps(D1)}, D2={ps(D2)})")
        _print (f"\t|  Ra(C,D1)={ra1} Ra(C,D2)={ra2} Rb(C,D1)={rb1} Rb(C,D2)={rb2}")
        _print (f"\t|  Rb(C,D1)&Rb(C,D2)={r1} Ra(C,D1)&Rb(C,D2)={r2} Ra(C,D2)&Rb(C,D1)={r3}")
        x=ra1+ra2+rb1+rb2
        y=r1+r2+r3

        _print (f"\t-->f_b(C={ps(C)}, D1={ps(D1)}, D2={ps(D2)})={x}-{y}={x-y}")
        return x-y

    #-----------------------------------------------------------------------------

    #v16
    def varphi2a(C,D1,D2,X):
        if Blabel not in C:        
            l=difflen2(X,C)
            f=fa(C,D1,D2,X)         
            return f"{l}-{f}={l-f}", l-f
        return "0",0

        # if Blabel not in C: 
        #   return 2**difflen(X,C) - fa(C,D1,D2,X) 
        # return 0
        
    #v16 
    def varphi2b(C,D1,D2,X):    
        if Alabel not in C:        
            l=2**difflen(X,C)
            f=fb(C,D1,D2,X)         
            return f"{l}-{f}={l-f}", l-f
        
        return "0",0
        
    #v16    
    def varphi2(C,D1,D2,X): 
        _print(f"In varphi_2(C={ps(C)},D1={ps(D1)},D2={ps(D2)})")

        st,a=varphi2a(C,D1,D2,X)    
        _print(f"|  varphi_2^a(C={ps(C)},D1={ps(D1)},D2={ps(D2)})={st}")

        st,b=varphi2b(C,D1,D2,X)    
        _print(f"|  varphi_2^b(C={ps(C)},D1={ps(D1)},D2={ps(D2)})={st}")

        _print(f"--> varphi_2(C={ps(C)},D1={ps(D1)},D2={ps(D2)})={a}+{b}={a+b}")

        return a+b

    #v16
    def varphi3(C,D,X): 
        if len(C)>1 and len(D)>1:
            _print(f"In varphi)3({ps(C)},{ps(D)})")
            st,b = varphi2b(set(),C,D,X)    
            _print(f"|  varphi_2^b(C=,D1={ps(C)},D2={ps(D)})={st}")      
            return st,b
        return "0 (zewn.krawedz)",0




    #bijective case 
    def WInternal(CL,DL,C,D):
        C1,C2=CL
        D1,D2=DL
        lx=2**len(X)
        
        s1,s2=CL
        if abset(s1): return lx-2**(len(s1))
        if abset(s2): return lx-2**(len(s2))
        if aset(s1) and bset(s2) or  aset(s2) and bset(s1): 
            return lx-2**len(s1)-2**len(s2)+2

        s1,s2=DL
        if abset(s1): return lx-2**(len(s1))
        if abset(s2): return lx-2**(len(s2))
        if aset(s1) and bset(s2) or  aset(s2) and bset(s1): 
            return lx-2**len(s1)-2**len(s2)+2

        if aset(C) or bset(C):
            return lx-2**len(D1)-2**len(D2)-2**len(C1)-2**len(C2)+6

        raise Exception("Unknown variant")

    def WExternal(DL,C):    

        s1,s2=DL
        lx=2**len(X)

        
        if abset(s1): return lx-2**(len(s1))
        if abset(s2): return lx-2**(len(s2))
        if aset(s1) and bset(s2) or  aset(s2) and bset(s1): 
            return lx-2**len(s1)-2**len(s2)+2

        if len(C)==1 and (aset(C) or bset(C)):
            return lx-2**len(s1)-2**len(s2)+3

        raise Exception("Unknown variant")

    def Wbijective(CL,DL,C,D):
        #print("\nWbijective:", CL,DL,C,D,file=sys.stderr)
        if len(CL)==1: return WExternal(DL,C)
        if len(DL)==1: return WExternal(CL,D)
        return WInternal(CL, DL, C, D)

    for rid in dur:
        CL,DL,C,D, start = dur[rid]

        
    

        X=(C.union(D)).difference(ABset)

        _print("="*100,X)
        _print (f"Krawedz: {CL}---{DL},  X={X}",color='cyan')   

        _print (f"1. NO-ARROW ---------------------------",color='red') 
        _varphi1 = varphi1(C,D,X)
        _print (f"No-arrow wynik: varphi_1({ps(C)},{ps(D)})={_varphi1}",color='red')    

        Q0 = _varphi1 
            
        _print(f"\n2. ONE-ARROW ----------------------",color='red')    
        p2c=0
        p2d=0 

        if len(DL)==2:         
            p2c=varphi2(C,DL[0],DL[1],X)
            _print(f"One-arrow case I: varphi_2({ps(C)},{ps(DL[0])},{ps(DL[1])})={p2c}",color='red')

        if len(CL)==2: 
            p2d=varphi2(D,CL[0],CL[1],X)
            _print(f"One-arrow case II: varphi2({ps(D)},{ps(CL[0])},{ps(CL[1])})={p2d}",color='red')

        Q1=p2c+p2d        
        _print(f"\n3. DOUBLE-ARROW --------------------",color='red')
        st,Q2 = varphi3(C,D,X)  
        _print(f"Double-arrow edge: varphi3({ps(C)},{ps(D)})={st}",color='red')

        _print(f"Res: NoA={Q0} One={Q1} Dbl={Q2} edge {CL}---{DL} ",color='blue')
        # print(f"Res: NoA={Q0} One={Q1} Dbl={Q2} edge {CL}---{DL} ")

        total = Q0+Q1+Q2
        if skipzeros and not total: continue
        
        #print(f"{rid},{Q0},{Q1},{Q2},{total},{start},\"{psx(*CL)}---{psx(*DL)}\"")
        yield rid,Q0,Q1,Q2,total,start