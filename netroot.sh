#!/bin/bash

export CSVADDINFOCOLUMNS=4
JOBS=5

function Usage()
{
cat <<EOF
Usage: $0 OPTIONS
 -n NETWORK
 -g GENETREE 
 -G FILE/DIR gen dot file of the gene tree and its network; DIR with Csv multiprocessing
 -p SCALE - gen dot->pdf when using -G with given scale factor
 -a [012]+ - type of algorithm (0: basic, 1: general, default 2: general + network contractions)
 -r - use simple LABEL=VALUE format when evaluating a gene tree and a single network (default is a csv format)
 -R - original root is present in the newick notation of a gene tree (def. not present)
 -f - print only the resulting gene tree with supports (simple output)
 -E eb0B - in NHX representation: e-add edge id, b-add best=1, 0-skip supports with 0, B-only best
 -h help

Csv multitest
 -c CSVFILE - input file
    csvheader EOLN 
    each line: k-info_columns,Network,GeneTree
 -s NUM - analyse just one pair with id=NUM 
 -k NUM - the number of info columns in csv file; the default is $CSVADDINFOCOLUMNS
 -j NUM - the number of jobs for parallel processing csv file; default is $JOBS
 -x skip parallel, just merge data
EOF

} 

if [[ "$*" = "" ]]
then
    Usage 
    
    cat << EOF 

$0 -n "((((((t36,t9),(#C,(t51,(t24)#A))),(t23,t53)),t45),((t57,((#E,(#A,(((t38,t28),t6),(#F,((t26,(t25)#H),t49))))),(#B,(t2,(t33)#B)))),((t31,(t42,(t4)#G)),t47))),((((((t16,t44),(t52,t3)),((t54,(t20,(t59,t37))),t11)),(t34,t18)),(((t48,(t32,t30)),(t35,(t14)#F)),((t17,((t39,t13),t27)),t15))),(#G,((((t50,t19),(t7)#E),t60),((t21,(t41,((t29,(t46)#C),(#D,(t10,t58))))),((t22,(t8,t43)),(#H,(((t12,(t5)#D),(t55,t40)),(t56,t1)))))))))" -g "(((((((t7,t19),t50),((t60,t60),((t50,t19),t7))),(((((t55,(t12,t5)),(t56,t1)),t25),((t22,(t8,t43)),(t22,(t8,t43)))),(t21,(t41,((t58,t10),(t46,t29)))))),((t34,t18),(((t3,t3),(t11,(t54,((t37,t59),t20)))),(t52,(((t16,t44),t16),((t16,t16),t44)))))),(t35,((t32,t30),t48))),((((((((t24,t24),t24),((((t14,t49),t6),(t28,t38)),t26)),(t33,t2)),((t33,t2),((((t14,((t6,t6),t28)),t26),t49),t24))),(t33,t2)),((t57,((t42,(t47,t31)),t4)),(t57,t57))),((((((t53,t23),(t9,(t36,t36))),(t53,t23)),t51),t45),t45)))"

$0 -g "(((t10,(t19,(t19,(t19,t19)))),(((t14,(t14,t14)),t14),t14)),((t18,t6),((t4,t7),((t5,((t17,(t15,t15)),t15)),t2))))" -n "(t14,(((t9,(t1,(t3,t20))))#B,((t19)#D,((((t16)#A,(#B,t12)),((t5)#C,(t13,t10))),(#A,((t11,(t6,t18)),((t4,(#D,t7)),(t2,(#C,((t17,t8),t15))))))))))"

$0 -n "(a,(b,c))" -g "(a,(b,c))" -a012 

EOF
    exit 20
fi

set -- `getopt G:i:I:g:n:hdzc:j:k:p:s:o:xa:qrRfE: $*`

[[ -d embretnet ]] || git clone git@bitbucket.org:pgor17/embretnet
[[ -d urec ]] || ( git clone git@bitbucket.org:pgor17/urec && cd urec && make )
[[ -d csvmanip ]] || ( git clone git@github.com:ppgorecki/csvmanip.git )

EMBNET=embretnet/embnet.py
export PATH=urec/urec:$PATH
export ORIGINALROOT=

OPT=
SHOWRESULT=1
GENETREEONLY=
PDFSCALE=0.5

while [ "$1" != -- ]
do
    case $1 in
        -h)   HFLG=1;;
        -n)   NETWORK=$( $EMBNET -n $2 -pn ); shift;;
	    -g)   GENETREE=$( $EMBNET -n $2 -pn ); shift;; 	
        -j)   JOBS=$2; shift;;  		
        -c)   MULTISETCSVFILE=$2                 
                shift;;
        -q)   SHOWRESULT= ;;
        -k)   CSVADDINFOCOLUMNS=$2; shift;;
        -G)   GENDOTFILES=$2; shift;;
        -p)   GENPDF="-p$2";  shift;;
        -s)   SINGLECSVPAIR=$2; shift;; 
        -o)   OUTPUTRES=$2; shift;; 
        -x)   SKIPPARALLEL=1;;
        -a)   ALGTYPE=$2; 
                [[ $ALGTYPE == *0* ]] && export A0=1
                [[ $ALGTYPE == *1* ]] && export A1=1
                [[ $ALGTYPE == *2* ]] && export A2=1                
                shift;;
        -r)   RAWFORMAT=1;;
        -f)   GENETREEONLY=1; SHOWRESULT=;;
        -R)   ORIGINALROOT='-R'; OPT=" $OPT -R";;
        -E)   GENETREEWITHEDGEIDSOPS='-E$2'; OPT=" $OPT -E$2"; shift;;

        #passing args to absupport

        -z)   OPT=" $OPT -z";; 
        -i)   OPT=" $OPT -i \"$2\""; shift;;
        -I)   OPT=" $OPT -I \"$2\""; shift;;       
    esac
    shift   
done
        
if ! [[ $A0 || $A1 || $A2 ]]
then
    export A1=1
fi

runsingle()
{
    set $1
    num=$1
    dir=$3
    csvaddinfoheader=$4
    GENDOTFILES=$5
    GENPDF=$6

    i=$( cut -f-$CSVADDINFOCOLUMNS -d',' <<< $2 )

    (( p=CSVADDINFOCOLUMNS+1 ))
    
    set $( echo $2 | cut -f$p- -d',' | sed 's/","/ /g; s/[";]//g;' )
    n=$1
    g=$2
    [[ $GENDOTFILES ]] && GENDOTFILES="-G$GENDOTFILES/$num"
    ./netroot.sh -i $i -I $csvaddinfoheader -n $n -g $g $GENDOTFILES $ORIGINALROOT $GENETREEWITHEDGEIDSOPS $GENPDF -o $dir/$num.res -q
}

export -f runsingle
export srcdir=$PWD

function csvmanip()
{
    $srcdir/csvmanip/csvmanip.py -c -i SourceFile,EdgeData,Network,NetworkContracted,GeneTree,GeneTreeLabels -l DL,H,n,ILS,Network,r -m "EDiameter" -f 'ORid,|E(G)|,|Labels(G)|,EDistHist' -n 'ORid,EDistHist,EDiameter,|Labels(G)|,|E(G)|,DL,H,n,r,ILS,Network' $*
}

if [[ $MULTISETCSVFILE ]]
then
    
    echo Processing $(( $( cat $MULTISETCSVFILE | wc -l ) -1 )) network-gene tree pairs from $MULTISETCSVFILE
    bn=$( basename $MULTISETCSVFILE .csv)
    
    dir=/tmp/$bn.netroot

    if ! [[ $SKIPPARALLEL ]]
    then
        rm -rf $dir
        mkdir -p $dir
    fi

    [[ $GENDOTFILES ]] && mkdir -p $GENDOTFILES


    # take first items from csv header
    csvaddinfoheader=$(  head -1 $MULTISETCSVFILE | cut -f-"$CSVADDINFOCOLUMNS" -d"," )

    if [[ $SINGLECSVPAIR ]]; then FL=$SINGLECSVPAIR; else FL=1; fi

    if ! [[ $SKIPPARALLEL ]]
    then

        tail -n +2 $MULTISETCSVFILE | sed 's/[";]/"/g;' | if [[ $SINGLECSVPAIR ]]
            then
                sed -n "$SINGLECSVPAIR"p
            else
                cat 
        fi | sed "s#.*#& $dir $csvaddinfoheader $GENDOTFILES $GENPDF #g" | nl -v $FL | parallel --progress --no-run-if-empty -j $JOBS runsingle
    fi

    echo Done $MULTISETCSVFILE
    echo Merging data    
    
    cd $dir

    csvmanip *res > $srcdir/$bn.res.csv        
    
    cd - &>/dev/null
    echo Summary file $bn.res.csv created
    exit $?
fi

shift   

if [[ "$HFLG" || -z $NETWORK || -z $GENETREE ]]
then
    Usage 
    exit 0
fi

function compabdelta()
{
    NT=$1
    GT=$2
    OUTPUTFILE=$3
    AROPT=$4    
    
    readarray -t delta < <( $EMBNET -g $GT -n $NT -pA$AROPT )   
    abdeltas=''
    # echo "Variants: ${#delta[@]}"    
    for i in "${delta[@]}"
    do        
        set $i
        abdeltas="$abdeltas $2;$3"
    done    
    # echo Running "absupports.py -l -A \"$abdeltas\" -g \"$GT\" -R  $OPT "
    absupports.py -l -A "$abdeltas" -g $GT -e $GENETREEWITHEDGEIDS $GENETREEWITHEDGEIDSOPS $ORIGINALROOT $OPT -u $URECROOTINGDATA -c $URECCONNECTIVITYDATA >> $OUTPUTFILE


}

[[ $OUTPUTRES ]] || OUTPUTRES=/tmp/$$

rm -f $OUTPUTRES $OUTPUTRES.1 $OUTPUTRES.2 $OUTPUTRES.0

URECROOTINGDATA=$OUTPUTRES.u
URECCONNECTIVITYDATA=$OUTPUTRES.c

urec/urec -g $GENETREE -rrxt > $URECROOTINGDATA
urec/urec -g $GENETREE -rc > $URECCONNECTIVITYDATA
GENETREEWITHEDGEIDS=$( urec/urec -g $GENETREE -Mi -p )


ALGS=0
if [[ $A0 ]]
then
    echo "# Basic rooting algorithm. Root delta's only" >> $OUTPUTRES.0
    echo "A0:" >> $OUTPUTRES.0
    compabdelta $NETWORK $GENETREE $OUTPUTRES.0 r 
fi

if [[ $A1 ]]
then
    echo "# General rooting algorithm" >> $OUTPUTRES.1
    echo "A1:" >> $OUTPUTRES.1
    compabdelta $NETWORK $GENETREE $OUTPUTRES.1
fi

GENELABELS=$( echo $( echo $GENETREE | tr '(,)' '\n' | sort | uniq  ) | tr ' ' ',' )

if [[ $A2 ]]
then
    echo "# General rooting algorithm + initial network contraction to L(G)" >> $OUTPUTRES.2
    echo "A2:" >> $OUTPUTRES.2    
    NETWORK2=$($EMBNET -n $NETWORK -R -P $GENELABELS -pu )
    # echo "$EMBNET -n \"$NETWORK\" -R -P $GENELABELS -pu "
    compabdelta $NETWORK2 $GENETREE $OUTPUTRES.2
fi

function compareres()
{
    F1=$OUTPUTRES.$1
    F2=$OUTPUTRES.$2
    
    EGNEQ=0
    EGEQ=0
    while read a b; 
    do 
        if [[ $a = $b ]] 
        then    
            (( EGEQ++ ))
        else    
            (( EGNEQ++ ))
        fi
    done < <(paste $F1 $F2 | grep "EdgeData" )

    SummariesEQ=True
    while read a b; do 
    if ! [[ $a = $b ]] 
    then    
        SummariesEQ=False
    fi
    done < <(paste $F1 $F2 | grep "OR" )

cat << EOF >> $OUTPUTRES
EdgeEq$1$2=$EGEQ    
EdgeNeq$1$2=$EGNEQ
SummariesEQ$1$2=$SummariesEQ
EOF

}

###

[[ $A0 && $A1 ]] && compareres 0 1
[[ $A1 && $A2 ]] && compareres 1 2
[[ $A0 && $A2 ]] && compareres 0 2

####

cat << EOF >> $OUTPUTRES
Network=$NETWORK
GeneTree=$GENETREE
GeneTreeLabels=$GENELABELS
EOF

if [[ $NETWORK2 ]]
then
    EqNets=False 
    [[ $NETWORK2 = $($EMBNET -n $NETWORK -pu ) ]] && EqNets=True
    TCNET=$( $EMBNET -n $NETWORK2 -pt )
    cat << EOF >> $OUTPUTRES
NetworkRed=$NETWORK2
IsNetRedTreeChild=$TCNET
EqNets=$EqNets
EOF
fi

[[ $A0 ]] && cat $OUTPUTRES.0 >> $OUTPUTRES
[[ $A1 ]] && cat $OUTPUTRES.1 >> $OUTPUTRES
[[ $A2 ]] && cat $OUTPUTRES.2 >> $OUTPUTRES

rm -f $OUTPUTRES.0 $OUTPUTRES.1 $OUTPUTRES.2

if [[ $SHOWRESULT ]]
then
    if [[ $RAWFORMAT ]]; 
    then
        cat $OUTPUTRES
    else
        csvmanip $OUTPUTRES
    fi
fi

# echo Result in $OUTPUTRES

if [[ $GENETREEONLY ]]    
then
    grep OutGeneTree $OUTPUTRES | cut -f2- -d'='    
fi

if [[ $GENDOTFILES ]]
then

    cnt=0
    [[ $A0 ]] && (( cnt++ ))
    [[ $A1 ]] && (( cnt++ ))
    [[ $A2 ]] && (( cnt++ ))

    if [[ $cnt -gt 1 ]]
    then
        echo Drawing only with one algorithm
        exit -1
    fi    

    DEST=$GENDOTFILES
    nodeshape=none    
    MAX=$( grep "BestSupp=" $OUTPUTRES | cut -f2 -d'=' )

    #########################
    # gene gene tree dot file
    urec -Mg -g $GENETREE -p | sed 's/# edgeid=\([0-9]*\)$/[label=\"E\1\"];/g' > $DEST.gt.dot
    
    sed '/^{.*/a edge [lblstyle="above,sloped"]; node [style="inner sep=1pt,minimum size=1pt"]; ' -i $DEST.gt.dot
    
    
    grep "EdgeData" $OUTPUTRES | cut -f2 -d'=' | tr "," " " | while read eid Q1 Q2 Q3 total start; 
    do
        penw=1
        #color="#808080"
        color="#000000" 
        style=solid
        srctotal=$total
        if [[ $total = 0 ]]
        then        
            total=
        else
            # penw=$( echo "scale=2; 2+3*$total/$MAX" | bc )            
            color="#000000" 
        fi                    

        if [[ $total == $MAX ]]
        then
            total="$total"
            color="#00FF00" # green - max            
        fi
        
        # mark OR if present 
        if [[ $ORIGINALROOT ]] && [[ $start == True ]];
        then            
            color="$color;0.4:#FF0000;0.2:$color;0.4" # does not work with dot2tex             
            total="$srctotal OR"                        
        fi    
        sed -i "s/label=\"E$eid\"/label=\"$total\",style=\"line width=$penw\",color=\"$color\"/g" $DEST.gt.dot
    done
    
    #nodeshape=circle
    
    # marking gamma (todo!)
    # for lf in $A1
    # do
    #     sed -i "s/shape=plaintext,label=\"$lf\"/shape=$nodeshape,label=\"$lf\",penwidth=3,color=\"#0000FF\"/g" $GENDOTFILES   
    # done
    # for lf in $A2
    # do
    #     sed -i "s/shape=plaintext,label=\"$lf\"/label=\"$lf\",penwidth=3,color=\"#00FF00\"/g" $GENDOTFILES
    # done    
    sed -i "s/shape=plaintext,/shape=$nodeshape,/g" -i $DEST.gt.dot

    [[ $GENPDF ]] && dot2tex  --graphstyle "scale="${GENPDF:2} --prog=neato --crop -ftikz --tikzedgelabels $DEST.gt.dot > $DEST.gt.tex && pdflatex -output-directory=$(dirname $DEST.net.tex) $DEST.gt.tex >/dev/null 2>&1 && >&2 echo $DEST.gt.pdf created 
        
    
    #************************
    # gen network dot file(s)
    
    function net2pdf()
    {
        F=$2 # dest basename 
        $EMBNET -n $1 -d$F -pi # gen $F.dot

        sed -i "s/\[\(.*\)\(#leaf\)/[shape=none,\1\2/g" $F.dot
        sed -i "s/\[\(.*\)\(#inner\)/[\1\2/g" $F.dot
        sed -i "s/label=\".*\"\(.*\)\(#retedge\)/label=\"\"\1\2/g" $F.dot # remove retedge labels
        sed -i "s/label=\"#\(.*\)\"\(.*\)\(#retic\)/label=\"\1\"\2\3/g" $F.dot # remove # from ret node labels
        sed -i '/digraph.*{.*/a edge [lblstyle="above,sloped"]; node [style="inner sep=1pt,minimum size=1pt"]; ranksep=0.1; nodesep=.25;' $F.dot

        [[ $GENPDF ]] && dot2tex --graphstyle "scale="${GENPDF:2} --prog=dot --crop -ftikz --tikzedgelabels $F.dot > $F.tex && pdflatex -output-directory=$(dirname $F.tex ) $F.tex >/dev/null 2>&1 && >&2 echo $F.pdf created 
    }

    net2pdf $NETWORK $DEST.net
    [[ $A2 ]] && net2pdf $NETWORK2 $DEST.netred

fi










