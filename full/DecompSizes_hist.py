#!/usr/bin/python3
import seaborn as sns
import sys
import matplotlib.pyplot as plt
sns.set_theme(style="whitegrid")
import math 

import pandas as pd

if len(sys.argv)>1:
    inputf = sys.argv[1];
else:
    inputf = '211214.res.csv'

print(f"Reading {inputf}")

src = pd.read_csv(inputf)
print("All",len(src))

src = src[src['|Labels(G)|'] >1 ]
print("All-rejected",len(src)) # ignore rejected

#set params
alg='A1:'
newcasesonly=True

print(f"Generate for {alg} and newcasesonly={newcasesonly}")    
vis="."

print("A1:GS supports>1",len(src[ src["A1:|PhiSupports|"]>1 ]))
print("A2:GS supports>1",len(src[ src["A2:|PhiSupports|"]>1 ]))

print("A1&A2:GS supports==1",len(src[ (src["A1:|PhiSupports|"]==1) & (src["A2:|PhiSupports|"]==1) ]))
print("A1:GS supports==1",len(src[ (src["A1:|PhiSupports|"]==1)  ]))

if newcasesonly:
    vis+="NC."
    src = src[ src[alg+"|PhiSupports|"]>1 ]

print ("Number of rows",len(src))
print ("Before draw:",len(src))

src['DLILS']=src['DL']+src['ILS']
src['n/r']=src['n'].astype(str) +"/"+src['r'].astype(str) 
src['n/r']=src['n'].astype(str) +"/"+src['r'].astype(str) 
for alg in ["A1:","A2:"]:
    for hue in ["n/r", "DLILS","r"]:
        g=sns.histplot(data=src, x=alg+"|PhiSupports|", binwidth=4, hue=hue, multiple='dodge') #,shrink=0.9)
        g.set_xlabel(r"The size of  $\Phi_G(N)$")        
        g.set(title=f"Algorithm={alg.replace(':','')}, Triplets={len(src)}")        

        destf=f"PhiSuppLens{alg.replace(':','')}{hue}x.pdf".replace(":","").replace("/","")
        plt.savefig(destf)
        print(f"{destf} saved")
        plt.clf()

for bw,r in [(1,4),(10,8)]:
    for alg in ["A1:","A2:"]:            
        srcx = src[ src["r"]==r ]
        g=sns.histplot(data=srcx, x=alg+"|PhiSupports|", binwidth=bw,  multiple='dodge') #,shrink=0.9)
        if alg == "A1:":
            g.set_xlabel(r"The size of  $\Phi_G(N)$")       
        else:
            g.set_xlabel(r"The size of  $\Phi_G((N|_{L(G)})^*)$")       
        g.set(title=f"Algorithm={alg.replace(':','')}, Triplets={len(srcx)}, r={r}, binsize={bw}")        

        destf=f"GsbyR{alg.replace(':','')}{hue}{bw}.pdf".replace(":","").replace("/","")
        plt.savefig(destf)
        print(f"{destf} saved")
        plt.clf()

