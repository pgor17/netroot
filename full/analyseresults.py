#!/usr/bin/python3
import seaborn as sns
import sys
import matplotlib.pyplot as plt
sns.set_theme(style="whitegrid")
import math 

import pandas as pd
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

from pathlib import Path  

if len(sys.argv)>1:
    inputf = sys.argv[1];
else:
    inputf = '211214.res.csv'

print(f"Reading {inputf}")

src = pd.read_csv(inputf)

#src.to_csv(Path('allt.csv')) 
allt = len(src)
print("A0:Basic Alg  A1=General Rooting Algorithm A2=A1+network reduction")
print("All records",allt)
resolved = src[src['A0:GammaSupportsErr'].isnull() ]
print("A0:OK:",len(resolved)) 
print("A0:Rejected:",allt-len(resolved)) 

print("A0:ORisUnBest:",len(src[src['A0:ORisUnBest']==True]))
print("A0:ORisBest:",len(src[src['A0:ORisBest']==True]))
print("EqEq01", len(resolved[resolved['EdgeNeq01'] == 0 ])) 

res = resolved[resolved['SummariesEQ01'] == True ]

print("A0 == A1:", len(res)) # A0  == A1 alg on old resolved data! 45235
print("A0 == A1 == A2:", len(res[res['SummariesEQ12']==True]))  # 42198 

print("A0:Best==A1:Best",len(src[src['A0:Best']==src['A1:Best']]))
print("A1:Best==A2:Best",len(src[src['A1:Best']==src['A2:Best']]))
print("A1:Best!=A2:Best",len(src[src['A1:Best']!=src['A2:Best']]))

o=resolved[resolved['SummariesEQ12'] == False ] # 37 cases not equall between A1 and A2
print("A0 == A1 <> A2:", len(o))
o.to_csv(Path('37_cases_old_eq_a1_neq_a2.csv')) 
oTrue = o[o['A1:ORisUnBest']==True]
print("eq12 UnBests", len(oTrue[oTrue['A2:ORisUnBest']==True])) # 1 unique best agreement

print ("=== Koniec analizy poprzednich wyników - b.duża zgodność A0,A1,A2")
print()

print("="*80)

labg1=src[src['|Labels(G)|']==1]
print("LabG=1",len(labg1)) # 76 cases - tego nie da sie policzyc
# print(labg1)

print("="*80)
labg2=src[src['|Labels(G)|']>1] # 12289

newcases = src[src['A0:GammaSupportsErr'].notnull() ] # 12289 to samo
print("newcases", len(newcases)) # 12289 to samo

res = newcases[newcases['SummariesEQ12'] == True ]
print("Summaries: A1 == A2:", len(res))  # 9261

res = newcases[newcases['A1:Best'] == newcases['A2:Best'] ]
print("Best: A1 == A2:", len(res))  # 10688

res = newcases[newcases['A1:Best'] != newcases['A2:Best'] ]
print("Best: A1 != A2:", len(res))  # 1677

#################
neq12 = newcases[newcases['SummariesEQ12'] == False ]
print("Summaries A1 != A2:", len(neq12))  # 3028

res = newcases[newcases['EdgeNeq12'] == 0 ]
print("EdgeData A1 == A2:", len(res))  # 8588


#################

edgedataneq12 = newcases[newcases['EdgeNeq12'] > 0 ]
print("EdgeDataNeq A1 != A2:", len(edgedataneq12))  # 3777
print("  --> Summaries: A1 == A2:", len(edgedataneq12[edgedataneq12['SummariesEQ12'] == True ])) # 749

# Filter Eq summaries
fneq = edgedataneq12[edgedataneq12['SummariesEQ12'] == False ] # 749
print("  among 3777  NeqSummaries: A1 != A2:", len(fneq)) # 3028
print("  unbest 3028 --> ",len(fneq[(fneq['A1:ORisUnBest'] == True) & (fneq['A2:ORisUnBest'] == True)] )) # 292
fneq2 = fneq[(fneq['A1:ORisUnBest'] == False) | (fneq['A2:ORisUnBest'] == False)]
print("  unbestdomk -->",len(fneq2))


print("Unique best")
res1 = newcases[newcases['A1:ORisUnBest'] == True ]
print("  unbest1:", len(res1))  # 2793 
res2 = newcases[newcases['A2:ORisUnBest'] == True ]
print("  unbest2:", len(res2))  # 2769
res12 = newcases[(newcases['A2:ORisUnBest'] == True) & (newcases['A1:ORisUnBest'] == True)]
print("  unbest1and2:", len(res12))  # 2664
 
print("OR jest best:")
res1 = newcases[newcases['A1:ORisBest'] == True ]
print(" best1:", len(res1))  # 5687 
res2 = newcases[newcases['A2:ORisBest'] == True ]
print(" best2:", len(res2))  # 5659
res12 = newcases[(newcases['A2:ORisBest'] == True) & (newcases['A1:ORisBest'] == True)]
print(" best1and2:", len(res12))  # 5424


res1 = neq12[neq12['A1:ORisUnBest'] == True ]
print("unbest1:", len(res1))  # 421
res2 = neq12[neq12['A2:ORisUnBest'] == True ]
print("unbest2:", len(res2))  # 397

res12 = neq12[(neq12['A2:ORisUnBest'] == True) & (neq12['A1:ORisUnBest'] == True)]
print("unbest1and2:", len(res12))  # 292

res12rest = neq12[(neq12['A2:ORisUnBest'] == False) | (neq12['A1:ORisUnBest'] == False)]
print("unbest1or2:", len(res12rest))  # 2736

#################################################################
print("=="*80)

src = src[src['|Labels(G)|']>1 ]

def jaccard(row):
    a = set(row["A1:Best"].split(";"))
    b = set(row["A2:Best"].split(";"))
    return len(a.intersection(b))/len(a.union(b))

src['Jaccard12']=src.apply(lambda row: jaccard(row),axis=1)

print("all-nonrejected",(a:=len(src)))
print( "unbest",(u:=len(src[src['A1:ORisUnBest'] == True])))
print( "best",(b:=len(src[src['A1:ORisBest'] == True])))
print( "only best",b-u)
print( "FF",a-b)



