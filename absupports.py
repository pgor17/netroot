#!/usr/bin/python3 

import sys
import getopt
from embretnet.treeop import Tree, str2tree
from formulas import gtsupport, EdgeDistance, geturecrootingdata




def usage():
    print("""
Perform a rooting analysis for a given gene tree with a list of Phi supports.

See extremesplits.sh script for performing the main gene tree rooting inference.

Options:

-g STR - genetree 
-A "A1;A2;lev A1;A2;lev ..." - Phi supports with multiple levels (def. is "a;b;0")
-R use original root    
-z do not print supports of 0
-I LABEL1,LABEL2,... - list of additional variables (labels)
-i VALUE1,VALUE2,... - list of additional values 
-u FILE - urec rooting data for the gene tree; if not present urec will be executed
-c FILE - urec connectivity data for the gene tree; if not present urec will be executed
-e GENETREEWITHEDGEIDS - used to generate NHX representation of a gene tree with [&&NHX:rootsupp=VALUE].
-E [ebB0]+ - additional options in e:
  - e add edge id
  - b add best=1 
  - B only best rootsupp
  - 0 ignore 0 supports

Required packages:
git clone git@bitbucket.org:pgor17/urec.git
git clone git@bitbucket.org:pgor17/embretnet.git

Example: 

absupports.py -A "t2,t17;t16,t10,t3,t14,t20,t18,t6,t9,t19,t12;2 t10,t3,t18,t9,t17,t2;t16,t14,t20,t19,t12;2 t17;t16,t10,t3,t14,t20,t18,t6,t19,t9,t2,t12;2 t10,t3,t18,t9,t17;t16,t14,t20,t19,t2,t12;2" -g "((((((t17,t2),(t4,t13)),(t3,t18)),t9),t10),(((t12,t14),(t20,t16)),(t19,t6)))" -R -I A -i 13

""")

def remquaotes(s):
    if s and s[0]=='"'==s[-1]: return s[1:-1]
    return s

def parsePhiSupports(a):
        if not a: return []    
        return [  (frozenset(s[0].split(",")),frozenset(s[1].split(",")),int(s[2]))  for c in a.strip().split(' ') 
            if (s:=c.split(";")) ]

def main():

    try:
        opts, args = getopt.getopt(sys.argv[1:], "A:g:Rzi:I:lu:c:e:E:")
      
    except:
        print("Error in opt")
        usage()
        sys.exit(-1)

    genetree = None
    gtree = None
    origroot = 0
    skipzeros = 0
    csvaddinfo=""
    csvaddinfoheader=""
    urecconnectivitydata = None
    urecrootingdata = None
    genetreewithids = None
    genetreewithidsopts = ''
    eoptions = ""

    gammaSupports = parsePhiSupports("a;b;0")
    
    for opt, arg in opts:       
        if opt in ['-A']:        
            gammaSupports = parsePhiSupports(arg)                
        elif opt in ['-g']:        
            genetree = arg        
            gtree = Tree(str2tree(genetree))
        elif opt in ['-R']:        
            origroot = 1
        elif opt in ['-z']:        
            skipzeros = 1
        elif opt in ['-i']:        
            csvaddinfo = remquaotes(arg)
        elif opt in ['-e']:        
            genetreewithids = arg
        elif opt in ['-E']:        
            genetreewithidsopts = arg
        elif opt in ['-I']:        
            csvaddinfoheader = remquaotes(arg)
        elif opt in ['-u']:
            try:
                urecrootingdata = open(arg).read().strip().split("\n")
            except Exception:
                print(f"Cannot read urecrootingdata from {arg}",file=sys.stderr)
                sys.exit(-1)

        elif opt in ['-c']:
            try:
                urecconnectivitydata = open(arg).read().strip().split("\n")
            except Exception:
                print(f"Cannot read urecconnectivitydata from {arg}",file=sys.stderr)
                sys.exit(-1)                
        
        
    if not genetree:
        print("Gene tree is undefined")
        usage()
        exit(-1)

    if csvaddinfo and csvaddinfoheader:
        csvaddinfo = csvaddinfo.split(",")
        csvaddinfoheader = csvaddinfoheader.split(",")

        csvaddinfo = "\n".join(map(lambda a: a[0]+"="+a[1],zip(csvaddinfoheader,csvaddinfo)))
    elif csvaddinfo and not csvaddinfoheader:
        print("csvaddinfo header is undefined",file=sys.stderr)
        exit(-1)

        

    #check correctness of gamma supports
    labels = [ l.label for l in gtree.leaves() ]

    def errsummary(info):                
        
        print(f"""|E(G)|={len(gtree.nodes)-2}                
|Labels(G)|={len(set(labels))}
PhiNets={len(gammaSupports)}
{info}
{csvaddinfo}
""")    
        if origroot:
            print("""ORisBest=False
ORisUnBest=False""")      
        exit(-1)

    if not gammaSupports:
        print("Empty Phi supports",file=sys.stderr)
        errsummary("PhiSupportsErr=NoClasses")


    for deltaLeft,deltaRight,lev in gammaSupports:
        if not deltaLeft.intersection(labels) or not deltaRight.intersection(labels): 
            print("Not all labels from largest supports are present in a gene tree",file=sys.stderr)
            errsummary("PhiSupportsErr=IncompatibleGT")
            

    if not urecrootingdata: 
        urecrootingdata = geturecrootingdata(genetree)

    ed = EdgeDistance(genetree, urecconnectivitydata)
    #main comp. loop

    debug = False
    res = []
    first = True

    for deltaLeft,deltaRight,lev in gammaSupports:

        _res = list(gtsupport(genetree, deltaLeft, deltaRight, skipzeros, urecrootingdata))

        if debug:
            print(deltaLeft, deltaRight, lev)    
            print(_res)

        
        for i,(rid,Q0,Q1,Q2,total,start) in enumerate(_res):
            scaling = 1/2**lev
            Q0 = Q0*scaling
            Q1 = Q1*scaling
            Q2 = Q2*scaling
            total = total*scaling

            if len(res)==len(_res):
                (rid2,Q02,Q12,Q22,total2,start2) = res[i]            
                if rid!=rid2 or start!=start2:
                    raise Exception("Edges does not match in gene trees (in variants)")
            else:   
                res.append(None)         
                (Q02,Q12,Q22,total2) = (0,0,0,0) # initial 
        

            res[i] = (rid, Q0+Q02, Q1+Q12, Q2+Q22, total+total2, start)


    if debug: 
        print("Merged")
        print(res)

    bestsupport  = max(r[4] for r in res)
    bestsupportcnt  = sum(r[4]==bestsupport for r in res)
    bestids = [ r[0] for r in res if r[4] == bestsupport ]

    oroot = None

    if origroot:

        for rid,Q0,Q1,Q2,total,start in res:    
            if start: 
                oroot=rid,Q0,Q1,Q2,total,start
                break

        if not oroot: 
            origroot = False # eg. for trees (a,b,(c,d)) 

        else: 
            orsupport = oroot[4]
            orid = oroot[0]

        orpos0=0
        orpos1=0

    besteids=[] # best edges/rootings

    for rid,Q0,Q1,Q2,total,start in res:
        if total==bestsupport: besteids.append(rid)                    
        print(f"EdgeData={rid},{Q0:g},{Q1:g},{Q2:g},{total:g},{start}")                
        if origroot and not start:
            if orsupport>=total:
                orpos0+=1
            if orsupport>total:
                orpos1+=1

        if genetreewithids:
            
            clean = False
            dest = []
            if 'e' in genetreewithidsopts: 
                dest = [f"id={rid}"]            
            if '0' in genetreewithidsopts and total==0:                
                clean = True
            if 'B' in genetreewithidsopts and total!=bestsupport:
                clean = True
            if 'b' in genetreewithidsopts and total==bestsupport:
                dest+=[f"best=1"]
            if not clean:
                dest+=[f"rootsupp={total:g}"]

            if clean:
                genetreewithids = genetreewithids.replace(f"[id={rid}]","")
            else: 
                genetreewithids = genetreewithids.replace(f"[id={rid}]",f"[&&NHX:{':'.join(dest)}]")

    if origroot:
        SuppRatio=orsupport/bestsupport
        ORposbest=orpos0/(len(res)-1)
        ORposworst=orpos1/(len(res)-1)
        ORposavg=(ORposbest+ORposworst)*0.5

        # distances from orid and bestids
        dmin,davg,dmax = ed.minavgmaxposstats(orid,besteids)

    # the largest edge-distance
    ediameter = ed.ediameter()

    # histogram of edge-distances
    histstr = ed.histstr()


    print(f"""|E(G)|={len(res)}
|Labels(G)|={len(set(labels))}
|Best|={bestsupportcnt}
Best={";".join(str(i) for i in bestids)}
BestSupp={bestsupport:g}
OutGeneTree={genetreewithids}""")
    if origroot:
        print(f"""ORSupp={orsupport}
ORid={orid}
ORisBest={orsupport==bestsupport}
ORisUnBest={orsupport==bestsupport and bestsupportcnt==1}
ORSuppRatio={SuppRatio:g}
ORposbest={ORposbest:g}
ORposavg={ORposavg:g}
ORposworst={ORposworst:g}
ORSupp={orsupport:g}
DistMin={dmin:g}
DistAvg={davg:g}
DistMax={dmax:g}""")
    print(f"""EDiameter={ediameter:g}
EDistHist={histstr}
|PhiSupports|={len(gammaSupports)}
{csvaddinfo}""")            


if __name__ == "__main__":
    main()

