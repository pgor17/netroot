#!/usr/bin/python3
import seaborn as sns
import sys
import matplotlib.pyplot as plt
sns.set_theme(style="whitegrid")
import math 

import pandas as pd

if len(sys.argv)>1:
    inputf = sys.argv[1];
else:
    inputf = '211214.res.csv'

print(f"Reading {inputf}")

genenet = pd.read_csv(inputf)
allt = len(genenet)
print("ALL",allt)
genenet = genenet[genenet['|E|'] >=0 ]
print("Rejected:",allt-len(genenet))

vis="."

genenet = genenet[genenet['ORisBest']==False]
genenet = genenet[genenet['ORisUnBest']==False]
vis+="FF."

print ("Number of rows",len(genenet))

# 57600 - ALL
#   12365 - rejected
#   8624 - unique best  true/true
#   10286 - nonunique best  true/false
#   26325 - rest - false/false
#   -- 15591 - ORSupp!=0 ->  good 
#   -- rest ORSupp==0; ~10k

print ("Bef. draw:",len(genenet))

genenet['DLILS']=genenet['DL']+genenet['ILS']
genenet['n/r']=genenet['n'].astype(str) +"/"+genenet['r'].astype(str) 

for col in ("DistMin",'DistAvg'):
    rsum=0
    for index, row in genenet.drop_duplicates(subset=["DL"]).iterrows():
        a,b,c,d = row['DL'], row['ILS'],row["n"], row["r"]
        x = genenet
        x = genenet[genenet['DL']==a]
        
        b=c=d=''        
        fig, ax = plt.subplots()
        ax.set_xlim(0.8,7)
        ax.set_ylim(0,1600)
        rsum+=len(x)
        
        if col=="DistMin":
            g=sns.histplot(x=col,  data=x, binwidth=1.0, hue="n/r",multiple='dodge',shrink=0.9);
            g.set_xlabel("Minimal Error")
        else:
            g=sns.histplot(x=col,  data=x, binwidth=1.0, hue="n/r",multiple='dodge',shrink=.9);
            g.set_xlabel("Average Error")

        g.set(title=f"DL={a}, ILS=all, Triplets={len(x)}")        
        destf=f"{col}{a}{b}{c}{d}{vis}pdf"
        plt.savefig(destf)
        print(f"{destf} saved")
        plt.clf()
    print(f"Rows included in {col}: {rsum} (from total {len(genenet)})")


