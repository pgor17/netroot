#!/usr/bin/python3
import seaborn as sns
import sys
import matplotlib.pyplot as plt
sns.set_theme(style="whitegrid")
import math 

import pandas as pd

if len(sys.argv)>1:
    inputf = sys.argv[1];
else:
    inputf = '211214.res.csv'

print(f"Reading {inputf}")

genenet = pd.read_csv(inputf)
allt = len(genenet)
print("ALL",allt)
genenet = genenet[genenet['|E|'] >=0 ]
print("Rejected:",allt-len(genenet))

vis="."
genenet = genenet[genenet['ORisBest']==False]
genenet = genenet[genenet['ORisUnBest']==False]
vis+="FF."

print ("Number of rows",len(genenet))
print ("Bef. draw:",len(genenet))

genenet['DLILS']=genenet['DL']+genenet['ILS']
genenet['n/r']=genenet['n'].astype(str) +"/"+genenet['r'].astype(str) 
genenet['Number of optimal edges to |E(G)| ratio']=genenet['|Best|']/genenet['|E|']

rsum=0
for index, row in genenet.drop_duplicates(subset=["DL"]).iterrows():
    a,b,c,d = row['DL'], row['ILS'],row["n"], row["r"]
    x = genenet
    x = genenet[genenet['DL']==a]
    b=c=d=''    
    fig, ax = plt.subplots()    
    rsum+=len(x)    
    g=sns.violinplot(
             x ="n",
             hue="r",
             cut=0,
             bw=.01,             
             y ="Number of optimal edges to |E(G)| ratio",             
             data = x)
    
    g.set(title=f"DL={a}, ILS=all, Triplets={len(x)}")
    destf=f"bester{a}{b}{c}{d}{vis}pdf"
    plt.savefig(destf)
    print(f"{destf} saved")
    plt.clf()



