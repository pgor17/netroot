# Gene-tree rooting via phylogenetic networks

Based on COCOON 2022 paper "Rooting Gene Trees via Phylogenetic Networks" by J. Tiuryn, N. Rutecka and P. G?recki.

The support was provided by National Science Centre grant #2019/33/B/ST6/00737.

## Required packages
```
git clone git@bitbucket.org:pgor17/embretnet.git
git clone git@bitbucket.org:pgor17/urec.git
git clone git@github.com:ppgorecki/csvmanip.git
```

These packages are automatically cloned when running netroot.sh for the first time.

## Running

Results of rooting are divided into at most three sections depending on algorithm type:
* A0: basic rooting algorithm (-a0)
* A1: general rooting algorithm; default (-a1)
* A2: general rooting algorithm + network reduction (-a2)

Algorithm type can be mixed in a single run, e.g., by using -a01.

### A simple gene tree analysis using a general rooting algorithm (A1)

```
> ./netroot.sh -n '(((c)#A,a),(#A,b))' -g '(a,(b,c))' -f
(a[&&NHX:rootsupp=1],b[&&NHX:rootsupp=1],c[&&NHX:rootsupp=0])
```

### A simple gene tree analysis using all algorithms (A0 is not applicable here)

```
./netroot.sh -n '(((c)#A,a),(#A,(b,d)))' -g '(b,(b,d))' -f -a012
Not all labels from largest supports are present in a gene tree
(b[&&NHX:rootsupp=0.0],b[&&NHX:rootsupp=0.0],d[&&NHX:rootsupp=1.0])
(b[&&NHX:rootsupp=0.0],b[&&NHX:rootsupp=0.0],d[&&NHX:rootsupp=1.0])
```

### A simple gene tree analysis using A1. Show only best supports.

```
./netroot.sh -n '(((c)#A,a),(#A,(b,d)))' -g '(b,(b,d))' -f -EB
(b,b,d[&&NHX:rootsupp=1.0])
```

### A simple run for a network and a gene tree using general rooting algorithm (A1) with csv output and analysis of original root position from a gene tree G (-R). Columns with values corresponding to the algorithm type are prefixed with A1.
```
> ./netroot.sh -n '(((c)#A,a),(#A,b))' -g '(a,(b,c))'  -R
Id,|E(G)|,|Labels(G)|,ORid,EDiameter,EDistHist,A1:|Best|,A1:Best,A1:BestSupp,A1:OutGeneTree,A1:ORSupp,A1:ORSupp1,A1:ORisBest,A1:ORisUnBest,A1:ORSuppRatio,A1:ORposbest,A1:ORposavg,A1:ORposworst,A1:DistMin,A1:DistAvg,A1:DistMax,A1:|PhiSupports|
54448,3,3,0,1,3;3,2,0;1,1,"(a[&&NHX:rootsupp=1],b[&&NHX:rootsupp=1],c[&&NHX:rootsupp=0])",1.0,1,True,False,1,1,0.75,0.5,0,0.5,1,1
```

### A simple run for a network and a gene tree usign general rooting all algorithms (A0, A1, A2). Csv output.

```
> ./netroot.sh -n '(((c)#A,a),(#A,b))' -g '(a,(b,c))'  -R -a012
Id,EdgeEq01,EdgeNeq01,SummariesEQ01,EdgeEq12,EdgeNeq12,SummariesEQ12,EdgeEq02,EdgeNeq02,SummariesEQ02,NetworkRed,IsNetRedTreeChild,EqNets,|E(G)|,|Labels(G)|,ORid,EDiameter,EDistHist,A0:|Best|,A0:Best,A0:BestSupp,A0:OutGeneTree,A0:ORSupp,A0:ORSupp1,A0:ORisBest,A0:ORisUnBest,A0:ORSuppRatio,A0:ORposbest,A0:ORposavg,A0:ORposworst,A0:DistMin,A0:DistAvg,A0:DistMax,A0:|PhiSupports|,A1:|Best|,A1:Best,A1:BestSupp,A1:OutGeneTree,A1:ORSupp,A1:ORSupp1,A1:ORisBest,A1:ORisUnBest,A1:ORSuppRatio,A1:ORposbest,A1:ORposavg,A1:ORposworst,A1:DistMin,A1:DistAvg,A1:DistMax,A1:|PhiSupports|,A2:|Best|,A2:Best,A2:BestSupp,A2:OutGeneTree,A2:ORSupp,A2:ORSupp1,A2:ORisBest,A2:ORisUnBest,A2:ORSuppRatio,A2:ORposbest,A2:ORposavg,A2:ORposworst,A2:DistMin,A2:DistAvg,A2:DistMax,A2:|PhiSupports|
54575,3,0,True,3,0,True,3,0,True,"((a,(c)#A),(b,#A))",True,True,3,3,0,1;1;1,3;3,2,0;1,1,"(a[&&NHX:rootsupp=1],b[&&NHX:rootsupp=1],c[&&NHX:rootsupp=0])",1.0,1,True,False,1,1,0.75,0.5,0,0.5,1,1,2,0;1,1,"(a[&&NHX:rootsupp=1],b[&&NHX:rootsupp=1],c[&&NHX:rootsupp=0])",1.0,1,True,False,1,1,0.75,0.5,0,0.5,1,1,2,0;1,1,"(a[&&NHX:rootsupp=1],b[&&NHX:rootsupp=1],c[&&NHX:rootsupp=0])",1.0,1,True,False,1,1,0.75,0.5,0,0.5,1,1
```

### Algorithm A1 with LABEL=VALUE output.
```
> ./netroot.sh -n '(((c)#A,a),(#A,b))' -g '(a,(b,c))' -r -R
Network=(((c)#A,a),(#A,b))
GeneTree=(a,(b,c))
GeneTreeLabels=a,b,c
# General rooting algorithm
A1:
EdgeData=0,1,0,0,1,True
EdgeData=1,1,0,0,1,False
EdgeData=2,0,0,0,0,False
|E(G)|=3
|Labels(G)|=3
|Best|=2
Best=0;1
BestSupp=1
OutGeneTree=(a[&&NHX:rootsupp=1],b[&&NHX:rootsupp=1],c[&&NHX:rootsupp=0])
ORSupp=1.0
ORid=0
ORisBest=True
ORisUnBest=False
ORSuppRatio=1
ORposbest=1
ORposavg=0.75
ORposworst=0.5
ORSupp=1
DistMin=0
DistAvg=0.5
DistMax=1
EDiameter=1
EDistHist=3;3
|PhiSupports|=1

```



### Fields and variables

Fields for the EdgeData variables in LABEL=VALUE output describe supports for each edge of G:
*  id - identifier of the edge
*  the contribution of weight from no-arrow edges
*  the contribution of weight from one-arrow edges
*  the contribution of weight from double edges
*  the weight of the edge
*  bool: True iff the edge is the original rooting edge 

Variables:

*  Id - identifier of the pair
*  |E(G)| - the number of edges in G
*  |Labels(G)| - the number of labels in G
*  |Best| - the number of edges having the largest weight
*  Best - the list of all optimal edges separated by ;
*  ORSupp - the weight of the original rooting edge
*  ORid - id of the original rooting edge
*  ORisBest (bool) - True if the original root has the best weight
*  ORisUnBest (bool) - True if the original root has the best unique weight
*  ORSuppRatio - OR support / best support
*  ORposbest - normalized OR position in a sequence of edges sorted by supports (heighest)
*  ORposavg - normalized OR position in a sequence of edges sorted by supports (average)
*  ORposworst - normalized OR position in a sequence of edges sorted by supports (the worst)
*  DistMin - min edge-distance between original root edge and largest weight edges
*  DistAvg - max (as above)
*  DistMax - min (as above)
*  EDiameter - gene tree diameter (in edges) 
*  EDistHist - histogram of edge distances (semicolon is the delimiter)
*  |PhiSupports| - the number of networks from Phi function
*  GeneTree - the input gene tree
*  GeneTreeLabels - L(G)
*  Network - the input network
*  NetworkRed - the network after reduction (only for A2)
*  IsNetRedTreeChild (bool) - is the reduced network tree-child?

Variables for the multiple runs (e.g., -a012)
* EdgeEq01 - the number of edges with the same EdgeData in the results for alg. A0 and A1 
* EdgeNeq01 - the number of edges with the different EdgeData in the results for alg. A0 and A1 
* EdgeEq12 - similar to EdgeEq01, but for alg. A1 and A2
* EdgeNeq12 - similar to EdgeNeq01, but for alg. A1 and A2
* SummariesEQ01 - True if all OR* attributes are the same for A0 and A1 (similarly with the 12, 01)

In the case when the gene tree is not matching the root largest support clusters from the network, it is reported by PhiSupportsErr=NoClasses. Similarly, if the gene tree is single-labeled.
```
> ./netroot.sh -n '((a,b),(c,d))' -g '(x,y)'
Empty Phi supports
Id,|E(G)|,|Labels(G)|,A1:PhiNets,A1:PhiSupportsErr
55023,1,2,0,NoClasses
```

Similarly, if the gene tree is single-labeled.
```
> ./netroot.sh -n '((a,b),(c,d))' -g '(a,a)'
Empty Phi supports
Id,|E(G)|,|Labels(G)|,A1:PhiNets,A1:PhiSupportsErr
55150,1,1,0,NoClasses
```

For the basic algorithm (A0) it is reported by IncompatibleGT.
```
./netroot.sh -n '((a,b),(c,d))' -g '(a,b)' -a0
Not all labels from largest supports are present in a gene tree
Id,|Labels(G)|,A0:|E|,A0:PhiNets,A0:ORisBest,A0:ORisUnBest,A0:PhiSupportsErr
199702,1,1,1,False,False,IncompatibleGT
```

### Multitest

Multitest from csv file using gnu parallel. Partial results in `/tmp/211214_100.netroot` directory: one file per pair. Joint result in 211214_100.res.csv. Default algorithm: general rooting algorithm (a1).

```
> head -11 211214.csv > 211214_10.csv
> ./netroot.sh -c 211214_10.csv
Processing 10 network-gene tree pairs from 211214_10.csv

Computers / CPU cores / Max jobs to run
1:local / 16 / 5

Done 211214_10.csv
Merging data
Summary file 211214_10.res.csv created
```

Multitest for all algorithms:
```
> ./netroot.sh -c 211214_1.csv -a012
Processing 0 network-gene tree pairs from 211214_1.csv

Computers / CPU cores / Max jobs to run
1:local / 16 / 1

Done 211214_1.csv
Merging data
Summary file 211214_1.res.csv created
```


### Visualization (using dot)

Generate dot figures of network and a gene tree with supports in figures directory (-G). With -pSCALE netroot will create pdfs using dot2tex package (required). Set a proper SCALE parameter depending on the size of output graphs (e.g. 0.5 for small networks, 1.0 for larger ones).
```
./netroot.sh -n '((c,#P),(((d,#R))#P,((e)#R,(f,g))))' -g '((c,d),(e,(f,g)))' -a1 -Gfigures/smprooting -p1.0 -f
((e[&&NHX:rootsupp=0.0],(f[&&NHX:rootsupp=0.0],g[&&NHX:rootsupp=0.0])[&&NHX:rootsupp=1.0])[&&NHX:rootsupp=2.0],c[&&NHX:rootsupp=1.0],d[&&NHX:rootsupp=0.0])
```

As above + original root info (-R).
```
> ./netroot.sh -n '((c,#P),(((d,#R))#P,((e)#R,(f,g))))' -g '((c,d),(e,(f,g)))' -a1 -Gfigures/smprootinwithor -p1.0 -f -R
((e[&&NHX:rootsupp=0],(f[&&NHX:rootsupp=0],g[&&NHX:rootsupp=0])[&&NHX:rootsupp=1])[&&NHX:rootsupp=2],c[&&NHX:rootsupp=1],d[&&NHX:rootsupp=0])
figures/smprootinwithor.gt.pdf created
figures/smprootinwithor.net.pdf created
```

Using random trees/networks (via embretnet call)

```
> ./netroot.sh -n 'rand:40:5' -g 'rand:30:0' -a2 -Gfigures/rand -p1.0 -f -R
((((v[&&NHX:rootsupp=32],(u[&&NHX:rootsupp=32],(n[&&NHX:rootsupp=32],(a[&&NHX:rootsupp=32],(a1[&&NHX:rootsupp=0],t[&&NHX:rootsupp=0])[&&NHX:rootsupp=32])[&&NHX:rootsupp=32])[&&NHX:rootsupp=32])[&&NHX:rootsupp=32])[&&NHX:rootsupp=32],((q[&&NHX:rootsupp=0],w[&&NHX:rootsupp=0])[&&NHX:rootsupp=32],((b[&&NHX:rootsupp=0],p[&&NHX:rootsupp=0])[&&NHX:rootsupp=16],((c1[&&NHX:rootsupp=0],h[&&NHX:rootsupp=0])[&&NHX:rootsupp=16],e[&&NHX:rootsupp=16])[&&NHX:rootsupp=16])[&&NHX:rootsupp=32])[&&NHX:rootsupp=32])[&&NHX:rootsupp=32],((((d[&&NHX:rootsupp=0],d1[&&NHX:rootsupp=0])[&&NHX:rootsupp=0],i[&&NHX:rootsupp=0])[&&NHX:rootsupp=32],k[&&NHX:rootsupp=32])[&&NHX:rootsupp=32],(l[&&NHX:rootsupp=24],(((r[&&NHX:rootsupp=0],o[&&NHX:rootsupp=0])[&&NHX:rootsupp=16],(m[&&NHX:rootsupp=0],b1[&&NHX:rootsupp=0])[&&NHX:rootsupp=16])[&&NHX:rootsupp=24],(y[&&NHX:rootsupp=0],j[&&NHX:rootsupp=0])[&&NHX:rootsupp=24])[&&NHX:rootsupp=24])[&&NHX:rootsupp=32])[&&NHX:rootsupp=32])[&&NHX:rootsupp=32],(s[&&NHX:rootsupp=0],g[&&NHX:rootsupp=0])[&&NHX:rootsupp=16],(f[&&NHX:rootsupp=0],((z[&&NHX:rootsupp=0],c[&&NHX:rootsupp=0])[&&NHX:rootsupp=0],x[&&NHX:rootsupp=0])[&&NHX:rootsupp=0])[&&NHX:rootsupp=16])
figures/rand.gt.pdf created
figures/rand.net.pdf created
figures/rand.netred.pdf created
```

Multitest with pdfs created in figs dir; with -a2 the reduced network is also visualized.
```
> ./netroot.sh -c 211214_10.csv -a2 -Gfigs -p1.0 -r -R  
Processing 10 network-gene tree pairs from 211214_10.csv
...
Done 211214_10.csv
Merging data
Summary file 211214_10.res.csv created
```

### The example from the article 

Gene tree rooting
```
> ./netroot.sh -a2 -EB -f -n"((((b)#B,c))#A,((d,(#A,a)),(#B,e)))" -g"(a,(b,(c,c)))"
(a[&&NHX:rootsupp=0.5],b,(c,c))
```

Visualizations
```
> ./netroot.sh -R -Gfig -p.5 -a2 -EB -f -n"((((b)#B,c))#A,((d,(#A,a)),(#B,e)))" -g"(a,(b,(c,c)))"
(a[&&NHX:rootsupp=0.5],b,(c,c))
fig.gt.pdf created
fig.net.pdf created
fig.netred.pdf created
```



### Files:

* 211214.csv - simulated set of 57000 networks and gene trees
* 211214_100.csv - just the first 100 elements from 211214.csv
* netroot.sh - the main rooting script (bash)
* absupports.py - script for computing edge weights from the largest supports
* formulas.py - edge weight computation formulas
* cocoon22 directory - results from the conference version
* full directory - newest results obtained with basic and two general rooting algorithms


